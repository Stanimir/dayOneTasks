package main;

public class CustomObjectKeyBad {
	int key;

	public CustomObjectKeyBad(int key) {
		this.key = key;
	}

	@Override
	public int hashCode() {
		
		return 2;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			
			return true;
		
		if (obj == null)
			
			return false;
		
		if (obj instanceof CustomObjectKeyGood)
			
			return false;
		
		CustomObjectKeyBad other = (CustomObjectKeyBad) obj;
		
		if (key != other.key)
			
			return false;
		
		return true;
	}

}
