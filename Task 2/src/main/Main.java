package main;

import java.util.HashMap;
import java.util.Random;

public class Main {

	private static long getTimeGood() {
		long start = System.currentTimeMillis();
		HashMap<CustomObjectKeyGood, Integer> good = new HashMap<>();
		Random random = new Random();

		for (int i = 0; i < 10000; i++) {
			CustomObjectKeyGood goodKey = new CustomObjectKeyGood(random.nextInt(10000));
			good.put(goodKey, i);
		}

		long end = System.currentTimeMillis();
		return end - start;
	}
	
	private static long getTimeNotSoGood() {
		long start = System.currentTimeMillis();
		HashMap<CustomObjectKeyNotSoGood, Integer> good = new HashMap<>();
		Random random = new Random();

		for (int i = 0; i < 10000; i++) {
			CustomObjectKeyNotSoGood goodKey = new CustomObjectKeyNotSoGood(random.nextInt(10000));
			good.put(goodKey, i);
		}

		long end = System.currentTimeMillis();
		return end - start;
	}
	
	private static long getTimeBad() {
		long start = System.currentTimeMillis();
		HashMap<CustomObjectKeyBad, Integer> good = new HashMap<>();
		Random random = new Random();

		for (int i = 0; i < 10000; i++) {
			CustomObjectKeyBad goodKey = new CustomObjectKeyBad(random.nextInt(10000));
			good.put(goodKey, i);
		}

		long end = System.currentTimeMillis();
		return end - start;
	}

	public static void main(String[] args) {
		System.out.println("Good time = " + getTimeGood());
		System.out.println("Not so good time = " + getTimeNotSoGood());
		System.out.println("Bad time = " + getTimeBad());
	}

}
