package main;

public class CustomObjectKeyNotSoGood {
	int key;

	public CustomObjectKeyNotSoGood(int key) {
		this.key = key;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			
			return true;
		
		if (obj == null)
			
			return false;
		
		if (obj instanceof CustomObjectKeyNotSoGood)
			
			return false;
		
		CustomObjectKeyNotSoGood other = (CustomObjectKeyNotSoGood) obj;
		
		if (key != other.key)
			
			return false;
		
		return true;
	}

	@Override
	public int hashCode() {

		return key * 2 + 3;
	}
}
