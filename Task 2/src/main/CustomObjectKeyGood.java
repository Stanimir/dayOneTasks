package main;

public class CustomObjectKeyGood {
	int key;

	public CustomObjectKeyGood(int key) {
		this.key = key;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			
			return true;
		
		if (obj == null)
			
			return false;
		
		if (obj instanceof CustomObjectKeyGood)
			
			return false;
		
		CustomObjectKeyGood other = (CustomObjectKeyGood) obj;
		
		if (key != other.key)
			
			return false;
		
		return true;
	}

	@Override
	public int hashCode() {

		return key;
	}

}
