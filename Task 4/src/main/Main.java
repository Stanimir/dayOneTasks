package main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Scanner;
import java.util.stream.Stream;


public class Main {

	private static String[] processText(String text) {
		String[] result = text.split(" ");

		for (int i = 0; i < result.length; i++) {
			result[i] = result[i].replace("!", "");
			result[i] = result[i].replace(".", "");
			result[i] = result[i].replace(",", "");
			result[i] = result[i].replace(":", "");
			result[i] = result[i].replace("?", "");
		}

		return result;
	}

	public static void main(String[] args) throws IOException {
		HashMap<String, Integer> map = new HashMap<>();
		Scanner scaner = new Scanner(new File("resources/text"));
		String[] words = { "" };
		String text = scaner.useDelimiter("\\Z").next();
		scaner.close();
		words = processText(text);
		
		for (String word : words) {

			if (map.containsKey(word)) {
				map.put(word, map.get(word) + 1);
			} else {
				map.put(word, 1);
			}

		}

		System.out.println(map);
	}

}
