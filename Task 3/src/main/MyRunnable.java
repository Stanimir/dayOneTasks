package main;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class MyRunnable implements Runnable {
	private static ConcurrentHashMap<Integer, Integer> map = new ConcurrentHashMap<>();
	private int iterations;

	public ConcurrentHashMap<Integer, Integer> getMap() {
		return map;
	}

	public MyRunnable(int iterations) {
		super();

		this.iterations = iterations;
	}
	
	public void clearMap()
	{
		map =  new ConcurrentHashMap<>();
	}

	@Override
	public void run() {
		Random random = new Random();

		for (int i = 0; i < iterations; i++) {
			map.put(random.nextInt(iterations * 2), random.nextInt(iterations * 2));
		}

	}
}
