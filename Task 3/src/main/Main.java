package main;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	private static long getTime(HashMap<Integer, Integer> map, int n) {
		long start = System.nanoTime();
		Random random = new Random();

		for (int i = 0; i < n; i++) {
			map.put(random.nextInt(n * 2), random.nextInt(n * 2));
		}
		long end = System.nanoTime();

		if (n == 10000) {
			int counter = 0;

			for (Map.Entry<Integer, Integer> entry : map.entrySet()) {

				if (map.containsKey(entry.getKey()))
					counter++;

			}

			System.out.println("Number of keys that are also values sometimes maybe = " + counter);
		}

		return end - start;
	}

	private static void startThreads(MyRunnable runnable) {
		for (int i = 0; i < 3; i++) {
			new Thread(runnable).start();
		}
	}

	private static void printRepeatedWords(MyRunnable runnable)
	{
			int counter = 0;

			for (Map.Entry<Integer, Integer> entry : runnable.getMap().entrySet()) {

				if (runnable.getMap().containsKey(entry.getKey()))
					counter++;

			}

			System.out.println("Number of keys that are also values sometimes maybe = " + counter);
	}
	
	public static void main(String[] args) {

		HashMap<Integer, Integer> map = new HashMap<>();
		System.out.println(getTime(map, 10_000));

		map = new HashMap<>();
		System.out.println(getTime(map, 100_000));

		map = new HashMap<>();
		System.out.println(getTime(map, 1_000_000));

		System.out.println();
		System.out.println();

		long start = System.nanoTime();
		MyRunnable runnable = new MyRunnable(10_000);
		startThreads(runnable);
		while (runnable.getMap().size() < 10_000) {
			;
		}
		printRepeatedWords(runnable);
		
		long end = System.nanoTime();
		runnable.clearMap();
		System.out.println(end - start);

		start = System.nanoTime();
		MyRunnable runnable1 = new MyRunnable(100000);
		startThreads(runnable1);
		while (runnable1.getMap().size() < 100_000) {
			;
		}
		end = System.nanoTime();
		runnable.clearMap();
		System.out.println(end - start);

		start = System.nanoTime();
		MyRunnable runnable2 = new MyRunnable(1_000_000);
		startThreads(runnable2);

		while (runnable2.getMap().size() < 1_000_000) {
			;
		}
		end = System.nanoTime();
		runnable.clearMap();
		System.out.println(end - start);

	}

}
