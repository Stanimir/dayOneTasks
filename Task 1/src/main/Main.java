package main;

import java.util.HashMap;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		HashMap<String, String> colors = new HashMap<>();
		colors.put("green", "#2db92d");
		colors.put("red", "#ff0000");
		colors.put("blue", "#0000ff");
		colors.put("yellow", "#ffff00");
		colors.put("white", "#ffffff");
		colors.put("black", "#000000");
		colors.put("dark blue", "#001a33");
		colors.put("pink", "#ff99ff");
		colors.put("purple", "#cc33ff");
		colors.put("orange", "#ff6600");
		colors.put("gray", "#737373");
		colors.put("cyan", "#00ffff");
		colors.put("light green", "#ccffcc");
		colors.put("dark green", "#003300");
		colors.put("brown", "#663300");
		Scanner scanner = new Scanner(System.in);
		String input = "";
		while (!colors.containsKey(input)) {
			System.out.println("GIB THE COLOUR NAME");
			input = scanner.next();
		}
		System.out.println(colors.get(input));
		scanner.close();
	}

}
